import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';

import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';
import { ViacepModule } from '@brunoc/ngx-viacep';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EditContaPage } from '../pages/edit-conta/edit-conta';
import { LoginProvider } from '../providers/login/login';
import { ApiProvider } from '../providers/api/api';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { EditContaPageModule } from '../pages/edit-conta/edit-conta.module';
import { TirarFotoPageModule } from '../pages/tirar-foto/tirar-foto.module';
import { SessaoProvider } from '../providers/sessao/sessao';
import { EnderecoProvider } from '../providers/endereco/endereco';
import { FestaProvider } from '../providers/festa/festa';
import { RecuperarSenhaPageModule } from '../pages/recuperar-senha/recuperar-senha.module';
import { VerificarEmailPageModule } from '../pages/verificar-email/verificar-email.module';
import { IonInputScrollIntoViewModule } from 'ion-input-scroll-into-view';

@NgModule({
  declarations: [
    MyApp,
    HomePage,

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),    
    IonicStorageModule.forRoot(),
    ViacepModule,
    HttpModule,
    EditContaPageModule,
    RecuperarSenhaPageModule,
    VerificarEmailPageModule,
    TirarFotoPageModule,
    IonInputScrollIntoViewModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    GoogleMaps,
    LoginProvider,
    ApiProvider,
    Facebook,
    UsuarioProvider,
    SessaoProvider,
    EnderecoProvider,
    FestaProvider,
    InAppBrowser
  ]
})
export class AppModule {}
