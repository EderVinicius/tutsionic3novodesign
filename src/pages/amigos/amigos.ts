import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController, NavParams } from 'ionic-angular';
import { UsuarioProvider} from '../../providers/usuario/usuario';
import { SessaoProvider} from '../../providers/sessao/sessao';
/**
 * Generated class for the AmigosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-amigos',
  templateUrl: 'amigos.html',
})
export class AmigosPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    private usuarioProvider: UsuarioProvider,
    private sessao: SessaoProvider

  ) {
    this.sessao.get()
    .then(
      res => {
        if(res._id){
          console.log(res)
          this.friendId = res._id;
          this.loadAmigos(res._id);
        }  else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }
  seguidores = [];
  usuarios = [];
  friendId = '';
  nome = '';
  ionViewWillEnter(){
    console.log('ionViewDidLoad AmigosPage');

  }

  loadAmigos(id){
    console.log('listarAmigos');
    this.usuarioProvider.listarAmigos(id)
    .subscribe(
      response => {
        console.log(response._body)
        this.seguidores = JSON.parse(response._body);
        this.seguidores = this.seguidores.map(function(obj){
          let selected = obj.friends.filter(function(fr){return fr == id});
          obj= {
            _id:obj._id,
            name:obj.name,
            selected:(selected.length > 0)?true:false,
            profile_photo:obj.profile_photo[0],
            gender:obj.gender,
            nickname:obj.nickname,
            friends:obj.friends
          }

          return obj;
        });
        console.log(this.seguidores)
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    )
  }
  pesquisarUsuarios(ev:any){
    let id = this.friendId;
    if(this.nome){
      this.usuarioProvider.pesquisarUsuarioPorNome(this.nome)
    .subscribe(
      response => {
        console.log(response)
        this.usuarios = JSON.parse(response._body);
        this.usuarios = this.usuarios.map(function(obj){
          let selected = obj.friends.filter(function(fr){return fr == id});
          obj= {
            _id:obj._id,
            name:obj.name,
            selected:(selected.length > 0)?true:false,
            profile_photo:obj.profile_photo[0],
            gender:obj.gender,
            nickname:obj.nickname,
            friends:obj.friends
          }

          return obj;
        });
        console.log(this.usuarios)
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    )
    } else {
      this.usuarios = [];
    }

  }
  visualizarPerfil(id){
    let page = (id!=this.friendId)?'OutroPerfilPage':'PerfilPage';
    let data = (id!=this.friendId)?{id:id}:{};
    this.navCtrl.push(page,data);
  }
  seguirSeguidor(act,userId){
    let data = {act:act,userId:userId,friendId:this.friendId};
    this.usuarioProvider.controlarSeguidores(data)
    .subscribe(
      response => {
        let friend = this.seguidores.filter(function(obj){
          return obj._id == userId;
        })
        friend[0].selected = !friend[0].selected;
        console.log(this.seguidores)
        this.toast.create({message:response._body,duration:3000}).present();
      },
      error => {
        alert(error._body);
      }
    )
  }
  seguirUsuario(act,userId){
    let data = {act:act,userId:userId,friendId:this.friendId};
    this.usuarioProvider.controlarSeguidores(data)
    .subscribe(
      response => {
        let friend = this.usuarios.filter(function(obj){
          return obj._id == userId;
        })
        friend[0].selected = !friend[0].selected;
        console.log(this.usuarios)
        this.toast.create({message:response._body,duration:3000}).present();
      },
      error => {
        alert(error._body);
      }
    )
  }
}
