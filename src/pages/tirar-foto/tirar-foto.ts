import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
/**
 * Generated class for the TirarFotoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tirar-foto',
  templateUrl: 'tirar-foto.html',
  providers: [
    Camera,
  ]
})
export class TirarFotoPage {
  foto = "";
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public camera:Camera) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TirarFotoPage');
  }
  tirarFoto(tipo) {
    let typeF = (tipo === 'C') ? this.camera.PictureSourceType.CAMERA : this.camera.PictureSourceType.PHOTOLIBRARY;
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: typeF,
      allowEdit: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.foto = 'data:image/jpeg;base64,' + imageData;
      //this.img = imageData;
    }, (err) => {
      // Handle error
    });
  }
  fechar(){
    this.viewCtrl.dismiss();
  }
  ok(){

      let data = { foto: this.foto };
      this.viewCtrl.dismiss(data);
      //this.viewCtrl.onDidDismiss(data => {
        //console.log(data);
      //});
  }
}
