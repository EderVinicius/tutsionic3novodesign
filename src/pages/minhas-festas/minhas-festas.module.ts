import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasFestasPage } from './minhas-festas';

@NgModule({
  declarations: [
    MinhasFestasPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhasFestasPage),
  ],
})
export class MinhasFestasPageModule {}
