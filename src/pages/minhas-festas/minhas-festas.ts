import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController,AlertController,ToastController, NavParams } from 'ionic-angular';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider } from '../../providers/sessao/sessao';
/**
 * Generated class for the MinhasFestasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-minhas-festas',
  templateUrl: 'minhas-festas.html',
})
export class MinhasFestasPage {
  festas = [];
  owner_id = '';
  constructor(
    public toast:ToastController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    private sessao:SessaoProvider,
    private festaProvider:FestaProvider
  ) {

  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad MinhasFestasPage');
    this.sessao.get()
    .then(
      res=>{
        if(res){
          this.owner_id = res._id;
          this.listarFestas(res._id);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    );
  }
  listarFestas(usuario_id){
    this.festaProvider.listarFestasDoUsuario(usuario_id)
    .subscribe(
      response=>{
        this.festas = JSON.parse(response._body);
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    )

  }
  detalharFesta(id){
    let modal = this.modal.create('DetalhesFestaPage',{id:id});
    modal.present();
    modal.onDidDismiss(
      data=>{
        if (data) {
          if(data.owner_id) this.navCtrl.push('PerfilPage');
        }
      }
    )
  }
  deletarFesta(id){
    this.festaProvider.deletarFesta(id)
    .subscribe(
      response=>{
        console.log(response)
        this.toast.create({message:'Festa deletada',duration:3000}).present();
        this.listarFestas(this.owner_id);
      },
      error=>{
        console.log(error)
        alert(error._body);
      }
    )
  }
  deletar(id){
    let confirm = this.alertCtrl.create({
      title: 'Deseja excluir essa festa?',
      message: '',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            console.log('Ok');
            this.deletarFesta(id);
          }
        }
      ]
    });
    confirm.present();

  }

}
