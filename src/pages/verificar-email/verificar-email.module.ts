import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerificarEmailPage } from './verificar-email';

@NgModule({
  declarations: [
    VerificarEmailPage,
  ],
  imports: [
    IonicPageModule.forChild(VerificarEmailPage),
  ],
})
export class VerificarEmailPageModule {}
