import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, ModalController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the VerificarEmailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-verificar-email',
  templateUrl: 'verificar-email.html',
})
export class VerificarEmailPage {
  dados = {
    id:'',
    email:'',
    old_email:'',
    codigo:''
  }
  exibirCodigo = false;
  emailForm:any;
  codigoForm:any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast:ToastController,
    private formBuilder: FormBuilder,
    private usuarioProvider:UsuarioProvider) {
      if(this.navParams.data.email){
        this.dados.id = this.navParams.data.id;
        this.dados.old_email = this.navParams.data.email;
        this.dados.email = this.navParams.data.email;
      }
      this.emailForm = this.formBuilder.group({
        email:[this.dados.old_email,Validators.required]
      });
      this.codigoForm = this.formBuilder.group({
        codigo:[this.dados.codigo,Validators.required]
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerificarEmailPage');
  }
  enviarEmail(){
    let dados = {id:this.dados.id,old_email:this.dados.old_email,email:this.dados.email};
    this.usuarioProvider.enviarCodigoEmail(dados)
    .subscribe(
      response => {
        this.toast.create({message:response._body,duration:3000}).present();
        this.exibirCodigo = true;
      },
      error => {
        alert(error._body);
        this.exibirCodigo = false;
      }
    );

  }
  ativarConta(){
    let dados = {code:this.dados.codigo};
    this.usuarioProvider.ativarConta(dados)
    .subscribe(
      response => {
        this.toast.create({message:response._body,duration:3000}).present();
        this.exibirCodigo = true;
        this.closeModal();
      },
      error => {
        alert(error._body);
        this.exibirCodigo = false;
      }
    );
  }
  closeModal(){
    this.navCtrl.pop();
  }
}
