import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfigbPage } from './configb';

@NgModule({
  declarations: [
    ConfigbPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfigbPage),
  ],
})
export class ConfigbPageModule {}
