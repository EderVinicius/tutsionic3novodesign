import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { HomePage } from '../home/home';

/**
 * Generated class for the ConfigbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-configb',
  templateUrl: 'configb.html',
})
export class ConfigbPage {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public sessao:SessaoProvider
  )  {
    this.sessao.get()
    .then(
      res => {
        if(res){
          if(res.pessoa_fisica)this.navCtrl.push('ConfigPage')
          console.log(res);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }
  abrir(pagina){
    this.navCtrl.push(pagina);
  }
  logout(){
    this.sessao.remove();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigbPage');
  }

}
