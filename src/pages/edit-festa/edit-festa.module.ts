import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditFestaPage } from './edit-festa';

@NgModule({
  declarations: [
    EditFestaPage,
  ],
  imports: [
    IonicPageModule.forChild(EditFestaPage),
  ],
})
export class EditFestaPageModule {}
