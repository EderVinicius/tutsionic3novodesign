import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider} from '../../providers/sessao/sessao';
import { ViacepService, Endereco, CepError } from '@brunoc/ngx-viacep';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Select } from 'ionic-angular';
import { ViewChild } from '@angular/core';

/**
 * Generated class for the EditFestaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-festa',
  templateUrl: 'edit-festa.html',
})
export class EditFestaPage {
  @ViewChild('style_music_select') style_music_select: Select;
  estilos = [];
  publico = [];
  festa = {
    owner_id:'',
    title:'',
    date:'',
    picture:[
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png'
    ],
    time:{
      open:'',
      close:''
    },
    price_male:'0',
    min_old:'18',
    description:'',
    style_music:'',
    public: '',
    formatted_address:'',
    address:{
      cep:'',
      street:'',
      number:'',
      city:'',
      state:'',
      neighborhood:''
    },
    geometry: {
      location: {
          lat: 0,
          lng: 0
      }
    }
  };
  editFesta = this.formBuilder.group({
    titulo:[this.festa.title,Validators.required],
    data:[this.festa.date,Validators.required],
    abre:[this.festa.time.open,Validators.required],
    fecha:[this.festa.time.close,Validators.required],
    preco:[this.festa.price_male,Validators.required],
    idade_minima:[this.festa.min_old,Validators.required],
    descricao:[this.festa.description,Validators.required],
    estilosMusicais:[this.festa.style_music,Validators.required],
    publico:[this.festa.public, Validators.required],
    cep:[this.festa.address.cep,Validators.required],
    rua:[this.festa.address.street,Validators.required],
    numero:[this.festa.address.number,Validators.required],
    cidade:[this.festa.address.city,Validators.required],
    estado:[this.festa.address.state,Validators.required],
    bairro:[this.festa.address.neighborhood,Validators.required]
  })
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sessao:SessaoProvider,
    private festaProvider:FestaProvider,
    private viacep: ViacepService,
    private modal: ModalController,
    public toast: ToastController,
    private formBuilder: FormBuilder
  ) {
    this.estilos = this.festaProvider.estilosDeMusica();
    this.publico = this.festaProvider.publicos();
    this.sessao.get()
    .then(res=>{
      if(res){
        this.festa.owner_id = res._id;
        console.log(res);
      } else {
        alert('Usuário não logado')
        this.sessao.remove();
      }

    })
    .catch(
      error=>{
        alert('Erro: '+error);
      }
    )
  }
//eder
openSelect(){
  this.style_music_select.open();
}
setPublic(tipo){
  this.festa.public = tipo;
}
//eder
  ionViewDidLoad() {
    console.log('ionViewDidLoad EditFestaPage');
  }
  salvar(){
    this.festa.formatted_address = this.festa.address.street +', '+ this.festa.address.number+'+'+this.festa.address.city+'+'+this.festa.address.state;

    var date = this.festa.date.split('T');
    this.festa.date = date[0] + 'T' + this.festa.time.open + ':00.000Z';
    console.log(this.festa.date);
    this.festaProvider.buscarPorEndereco(this.festa.formatted_address)
    .subscribe(
      response=>{
        let results  = response.json();
        console.log(results.results);
        console.log(this.festa.formatted_address);
        this.festa.geometry = results.results[0].geometry;
        this.salvarFesta();
      },error=>{
        alert(error);
      }
    );
    console.log(this.festa);
    //if(!this.navParams.data.id){

    //} else {
      //this.editarFesta();
    //}
  }
  salvarFesta(){
    console.log(this.festa)
    this.festaProvider.salvarFesta(this.festa)
    .subscribe(
      response=>{
        console.log(response)
        this.toast.create({message:'Festa Criada com sucesso!',duration:3000}).present();
        this.festa.address.cep = '';
        this.festa.address.city= '';
        this.festa.address.neighborhood = '';
        this.festa.address.number = '';
        this.festa.address.state = '';
        this.festa.address.street = '';
        this.festa.date = '';
        this.festa.public = '';
        this.festa.style_music = '';
        this.festa.price_male = '';
        this.festa.picture = [
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png'
        ];
        this.festa.title = '';
        this.festa.description = '';
        this.festa.time.open = '';
        this.festa.time.close = '';
        this.festa.min_old = '';
        this.festa.geometry = {
          location: {
              lat: 0,
              lng: 0
          }
        };
        let go = true;
        if(go){
          this.navCtrl.push('MinhasFestasPage');
        }

      },
      error=>{
        console.log(error);
        alert('Erro: '+error._body);
      }
    )
  }
  editarFesta(){

  }
  inserirFoto(index){
    let modal = this.modal.create('TirarFotoPage');
    modal.present();
    modal.onWillDismiss(data=>{
      if(data){

        console.log(data);
        this.festa.picture[index] = data.foto;
      }
      console.log(index);
    })
  }
  completarEndereco(){
    this.viacep.buscarPorCep(this.festa.address.cep)
    .then( ( endereco: Endereco ) => {
      // Endereço retornado :)
      this.festa.address.street = endereco.logradouro;
      this.festa.address.city = endereco.localidade;
      this.festa.address.state = endereco.uf;
      this.festa.address.neighborhood = endereco.bairro;
      console.log(endereco);
    }).catch( (error: CepError) => {
      // Alguma coisa deu errado :/
      alert(error.descricao);
      console.log(error.descricao);
    });
  }
}



