import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider} from '../../providers/sessao/sessao';
import { ViacepService, Endereco, CepError } from '@brunoc/ngx-viacep';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Select } from 'ionic-angular';
import { ViewChild } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { InAppBrowser, InAppBrowserOptions  } from '@ionic-native/in-app-browser';

/**
 * Generated class for the NovaFestabPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-nova-festab',
  templateUrl: 'nova-festab.html',
})
export class NovaFestabPage {
  @ViewChild('style_music_select') style_music_select: Select;
  estilos = [];
  publico = [];
  usuario;
  event_enable = false;
  festa = {
    owner_id:'',
    title:'',
    date:'',
    picture:[
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png'
    ],
    time:{
      open:'',
      close:''
    },
    description:'',
    style_music:'',
    public: '',
    formatted_address:'',
    address:{
      cep:'',
      street:'',
      number:'',
      city:'',
      state:'',
      neighborhood:''
    },
    geometry: {
      location: {
          lat: 0,
          lng: 0
      }
    },
    parking : '',
    phone_number:[],
    min_old: 18,
    price_male: '',
    price_female:''
  };
  editFesta = this.formBuilder.group({
    titulo:[this.festa.title,Validators.required],
    data:[this.festa.date,Validators.required],
    abre:[this.festa.time.open,Validators.required],
    fecha:[this.festa.time.close,Validators.required],
    idade_minima:[this.festa.min_old,Validators.required],
    preco_masculino:[this.festa.price_male,Validators],
    preco_feminino:[this.festa.price_female,Validators.required],
    descricao:[this.festa.description,Validators.required],
    estilosMusicais:[this.festa.style_music,Validators.required],
    publico:[this.festa.public, Validators.required],
    cep:[this.festa.address.cep,Validators.required],
    rua:[this.festa.address.street,Validators.required],
    numero:[this.festa.address.number,Validators.required],
    cidade:[this.festa.address.city,Validators.required],
    estado:[this.festa.address.state,Validators.required],
    bairro:[this.festa.address.neighborhood,Validators.required]
  })
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private sessao:SessaoProvider,
    private festaProvider:FestaProvider,
    private viacep: ViacepService,
    private modal: ModalController,
    public toast: ToastController,
    private formBuilder: FormBuilder,
    public httpClient: HttpClient,
    private theInAppBrowser: InAppBrowser
  ) {
    this.estilos = this.festaProvider.estilosDeMusica();
    this.publico = this.festaProvider.publicos();
    this.sessao.get()
    .then(res=>{
      if(res){
        this.festa.owner_id = res._id;
        this.usuario = res;
        console.log(res);
        this.getProducts();
        this.getStatus();
      } else {
        alert('Usuário não logado')
        this.sessao.remove();
      }

    })
    .catch(
      error=>{
        alert('Erro: '+error);
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditFestaPage');
  }
  ionViewWillEnter(){
  }
  inserirEstacionamento(){
    let modal = this.modal.create('EstacionamentoPage');
    modal.present();
    modal.onWillDismiss(
      data=>{
        this.festa.parking = data;
      }
    );

  }
  //eder
  openSelect(){
    this.style_music_select.open();
  }
  //eder
  salvar(){
    this.festa.formatted_address = this.festa.address.street +', '+ this.festa.address.number;
    this.festaProvider.buscarPorEndereco(this.festa.formatted_address)
    .subscribe(
      response=>{
        let results  = response.json();
        console.log(results.results);
        console.log(this.festa.formatted_address);
        this.festa.geometry = results.results[0].geometry;
        this.salvarFesta();
      },error=>{
        alert(error);
      }
    )
    //if(!this.navParams.data.id){

    //} else {
      //this.editarFesta();
    //}
  }
  salvarFesta(){
    this.festaProvider.salvarFesta(this.festa)
    .subscribe(
      response=>{
        this.toast.create({message:'Festa Criada com sucesso!',duration:3000}).present();
        this.festa.address.cep = '';
        this.festa.address.city= '';
        this.festa.address.neighborhood = '';
        this.festa.address.number = '';
        this.festa.address.state = '';
        this.festa.address.street = '';
        this.festa.parking = '';
        this.festa.date = '';
        this.festa.public = '';
        this.festa.style_music = '';
        this.festa.price_female = '';
        this.festa.price_male = '';
        this.festa.picture = [
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png',
          'assets/imgs/IconeFestas.png'
        ];
        this.festa.title = '';
        this.festa.description = '';
        this.festa.time.open = '';
        this.festa.time.close = '';
        this.festa.min_old = 18;
        this.festa.geometry = {
          location: {
              lat: 0,
              lng: 0
          }
        };

        let go = true;
        if(go){
          this.navCtrl.push('MinhasFestasPage');
        }
      },
      error=>{
        alert('Erro: '+error);
      }
    )
  }
  editarFesta(){

  }
  inserirFoto(index){
    let modal = this.modal.create('TirarFotoPage');
    modal.present();
    modal.onWillDismiss(data=>{
      if(data){

        console.log(data);
        this.festa.picture[index] = data.foto;
      }
      console.log(index);
    })
  }
  completarEndereco(){
    this.viacep.buscarPorCep(this.festa.address.cep)
    .then( ( endereco: Endereco ) => {
      // Endereço retornado :)
      this.festa.address.street = endereco.logradouro;
      this.festa.address.city = endereco.localidade;
      this.festa.address.state = endereco.uf;
      this.festa.address.neighborhood = endereco.bairro;
      console.log(endereco);
    }).catch( (error: CepError) => {
      // Alguma coisa deu errado :/
      alert(error.descricao);
      console.log(error.descricao);
    });
  }
  setPublic(publico){
    this.festa.public = publico;
  }
  _url = 'https://webtuts.com.br/payment/?fn=';
  _response: Observable<any>;
  _products = [];
  getProducts(){
    const body = new HttpParams()
    .set('email', '');
    this._response = this.httpClient.post(this._url+'getproduct', body.toString(), {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')});
    this._response.subscribe(data => {
      if(data.status==200){
        for(let i in data.products){
          this._products.push({
            id:data.products[i].id,            
            name:data.products[i].name,
            active:data.products[i].active,
            price:data.products[i].price,
            events_limit:data.products[i].events_limit,
            validity:data.products[i].validity,
            image:data.products[i].image,
            description:data.products[i].description
          })
        }
      }
    })
  }
  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
}
  buyPlan(id){
    console.log('buyPlan()');
    const body = new HttpParams()
    .set('product_id', id)
    .set('user_id', this.usuario._id)
    .set('user_name', this.usuario.name)
    .set('user_email', this.usuario.email)
    .set('user_cpf_cnpj', this.usuario.cpf_cnpj)
    ;
    this._response = this.httpClient.post(this._url+'buyproduct', body.toString(), {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')});
    this._response.subscribe(data => {
      console.log('data: ', data);
      if(data.status==200){
        console.log('IF');
        let target = "_blank";
        this.theInAppBrowser.create(this._url+'pay&code='+data.payment_code[0],target,this.options);
      }else{
        console.log('ELSE');
        this.toast.create({message:'Erro no pagamento! Reveja seus dados cadastrais, obrigatório Nome e Sobrenome',duration:3000}).present();        
      }
    },
    (error: any) => {
      console.log('ELSE');
      this.toast.create({message:'Erro no pagamento! Reveja seus dados cadastrais, obrigatório Nome e Sobrenome',duration:8000}).present();
      console.log(error);
    })    
  }
  //
  getStatus(){
    const body = new HttpParams()
    .set('user_id', this.usuario._id);
    this._response = this.httpClient.post(this._url+'getstatus', body.toString(), {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')});
    this._response.subscribe(data => {
      if(data.status==200){
        console.log(data);
        if(data.permission){
          this.event_enable = true;
        }else{
          this.event_enable = false;
        }
      }
    })    
  }

}
