import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovaFestabPage } from './nova-festab';

@NgModule({
  declarations: [
    NovaFestabPage,
  ],
  imports: [
    IonicPageModule.forChild(NovaFestabPage),
  ],
})
export class NovaFestabPageModule {}
