import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController, NavParams, ModalController, LoadingController } from 'ionic-angular';
import { FestaProvider } from '../../providers/festa/festa';
import { Geolocation } from '@ionic-native/geolocation';
import { SessaoProvider } from '../../providers/sessao/sessao';
/**
 * Generated class for the DestaquesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-destaques',
  templateUrl: 'destaques.html',
})
export class DestaquesPage {
  festas = [];
  usuario = {_id:''};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal: ModalController,
    public toast: ToastController,
    private festaProvider: FestaProvider,
    public loadingCtrl: LoadingController,
    private geolocation:Geolocation,
    private sessao:SessaoProvider
  ) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          this.usuario = res;
          console.log(this.usuario);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    );
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad DestaquesPage');
    let loading = this.loadingCtrl.create({
      content: 'Carregando sua localização...'
    });

    loading.present();
    this.geolocation.getCurrentPosition()
    .then((resp)=>{
      this.festaProvider.buscarPorCordenadas(resp.coords.latitude,resp.coords.longitude)
      .subscribe(
        response => {

          let result = response.json();
          //console.log(result)
          let cidade = result.results[0].address_components[3].long_name;
          console.log('>>> ', result.results[0].address_components[4])
          this.listarDestaques(cidade.toString());
          loading.dismiss();
        },
        error => {
          console.log(error)
        }
      );
    })
    .catch((e)=>{
      console.log(e)
    })

  }

  detalharFesta(id){
    let modal = this.modal.create('DetalhesFestaPage',{id:id});
    modal.present();
    modal.onDidDismiss(
      data=>{
        if (data) {
          if(data.owner_id != this.usuario._id) this.navCtrl.push('OutroPerfilPage',{id:data.owner_id});
            else this.navCtrl.push('PerfilPage');
        }
      }
    )
  }

  listarDestaques(cidade){
    this.festaProvider.listarFestasDestaques(cidade)
    .subscribe(
      response => {
        this.festas = JSON.parse(response._body);
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    );
  }
}
