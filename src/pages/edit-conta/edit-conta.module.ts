import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditContaPage } from './edit-conta';

@NgModule({
  declarations: [
    EditContaPage,
  ],
  imports: [
    IonicPageModule.forChild(EditContaPage),
  ],
})
export class EditContaPageModule {}
