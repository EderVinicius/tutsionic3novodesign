import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, ToastController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the EditContaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-conta',
  templateUrl: 'edit-conta.html',
})
export class EditContaPage {
  usuario = {idfacebook:'',nome:'',email:'',password:'',profile_photo:'assets/imgs/avatar.png',confpassword:'',pessoaFisica:true,pessoaJuridica:false,cnpj:'',dataNasc:'',sexo:''};
  //nome = (this.usuario.pessoaJuridica)?"Nome da empresa":"Nome";
  editEmail = false;
  public form:any;
  message = {
    nome:'',
    email:'',
    password:'',
    cnpj:'',
    dataNasc:'',
    sexo:''
  }
  constructor(
    public navCtrl: NavController,
    private usuarioProvider:UsuarioProvider,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public toast: ToastController,
    public formBuilder: FormBuilder
  ) {


    if(this.navParams.data.idfacebook){
      this.usuario.idfacebook = this.navParams.data.idfacebook;
      this.usuario.profile_photo = this.navParams.data.foto;
      this.usuario.nome = this.navParams.data.nome;
      this.usuario.email = this.navParams.data.email;
      this.editEmail = (this.usuario.email)?false:true;
      this.usuario.password = this.navParams.data.idfacebook;
      this.usuario.confpassword = this.usuario.password;
    }
    this.formEmpresa(false);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditContaPage');
  }
  closemodal(){
    this.navCtrl.pop();
  }
  formEmpresa(pessoaJuridica){
    if(pessoaJuridica){
      this.form = this.formBuilder.group({
        pessoaJuridica:[true,Validators.required],
        nome: [this.usuario.nome, Validators.required],
        cnpj:[this.usuario.cnpj, Validators.required],
        email: [this.usuario.email, Validators.compose([Validators.email,Validators.required])],
        password: [this.usuario.password, Validators.compose([Validators.minLength(6), Validators.maxLength(20),Validators.required])],
        confpassword: [this.usuario.confpassword,Validators.compose([Validators.minLength(6), Validators.maxLength(20),Validators.required])]
      });
    } else {
      this.form = this.formBuilder.group({
        pessoaJuridica:[false,Validators.required],
        nome: [this.usuario.nome, Validators.required],
        dataNasc:[this.usuario.dataNasc, Validators.required],
        sexo:[this.usuario.sexo, Validators.required],
        email: [this.usuario.email,Validators.compose([Validators.email,Validators.required])],
        password: [this.usuario.password, Validators.compose([Validators.minLength(6), Validators.maxLength(20),Validators.required])],
        confpassword: [this.usuario.confpassword,Validators.compose([Validators.minLength(6), Validators.maxLength(20),Validators.required])]
      });
    }

  }
//

  //EDER
  validarCNPJ(cnpj) {
    console.log('validarCNPJ(cnpj):', cnpj);
    if(cnpj!=undefined){    
    cnpj = cnpj.replace(/[^\d]+/g,'');
    cnpj = cnpj.replace(/\D/g,'');
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    if (cnpj.length < 14 && cnpj.length < 15)
        return false;
    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1))
    {
        digitos_iguais = 0;
        break;
    }
    if (!digitos_iguais)
    {
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--)
        {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--)
        {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
    }else{
      return false;      
    }
  }
  //EDER
//
  editarConta(){
    let usuario;
    this.usuario.pessoaFisica = !this.usuario.pessoaJuridica;
    let pass = true;

    if(this.usuario.pessoaJuridica && !this.validarCNPJ(this.usuario.cnpj)){
        pass = false;
    }
    if(this.usuario.pessoaFisica){
      usuario = {
        idfacebook:this.usuario.idfacebook,
        profile_photo: [this.usuario.profile_photo],
        name:this.usuario.nome,
        email:this.usuario.email,
        password:this.usuario.password,
        dob:this.usuario.dataNasc,
        gender:this.usuario.sexo,
        pessoa_fisica:this.usuario.pessoaFisica
      }
    } else {
      usuario = {
        idfacebook:this.usuario.idfacebook,
        profile_photo: [this.usuario.profile_photo],
        name:this.usuario.nome,
        email:this.usuario.email,
        password:this.usuario.password,
        cpf_cnpj:this.usuario.cnpj,
        pessoa_fisica:this.usuario.pessoaFisica
      }
    }

    if(pass){
      this.usuarioProvider.salvarUsuario(usuario)
    .subscribe(
      response=>{
        //this.entrar();
        console.log(response._body);
        this.toast.create({message:'Conta criada, bem vindo!',duration:3000}).present();
        this.viewCtrl.dismiss({email:usuario.email,password:usuario.password});
      },
      error=>{
        alert(error._body);
        console.log(error._body);
      }
    )      
    }else{
      this.toast.create({ message: 'CNPJ INVÁLIDO', duration: 3000 }).present();
    }
    
  }
}
