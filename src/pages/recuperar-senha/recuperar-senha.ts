import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, ModalController, NavParams } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';

/**
 * Generated class for the RecuperarSenhaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recuperar-senha',
  templateUrl: 'recuperar-senha.html',
})
export class RecuperarSenhaPage {
  dados = {
    email:'',
    codigo:'',
    senha:'',
    confsenha:''
  }
  exibirCodigo = false;
  emailForm = this.formBuilder.group({
    email:[this.dados.email,Validators.required]
  });
  senhaForm = this.formBuilder.group({
    senha:[this.dados.senha,Validators.required],
    confsenha:[this.dados.confsenha,Validators.required],
    codigo:[this.dados.codigo,Validators.required]
  });
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast:ToastController,
    private formBuilder: FormBuilder,
    private usuarioProvider:UsuarioProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RecuperarSenhaPage');
  }
  enviarEmail(){
    let dados = {email:this.dados.email};
    this.usuarioProvider.enviarCodigoSenha(dados)
    .subscribe(
      response => {
        this.toast.create({message:response._body,duration:3000}).present();
        this.exibirCodigo = true;
      },
      error => {
        alert(error._body);
        this.exibirCodigo = false;
      }
    );

  }
  alterarSenha(){
    let dados = {code:this.dados.codigo,password:this.dados.senha};
    this.usuarioProvider.atualizarSenha(dados)
    .subscribe(
      response => {
        this.toast.create({message:response._body,duration:3000}).present();
        this.exibirCodigo = true;
        this.closeModal();
      },
      error => {
        alert(error._body);
        this.exibirCodigo = false;
      }
    );
  }
  closeModal(){
    this.navCtrl.pop();
  }
}
