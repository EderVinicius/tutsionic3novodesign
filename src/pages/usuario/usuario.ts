import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { Usuario } from '../../providers/usuario/model-usuario';
/**
 * Generated class for the UserPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-usuario',
  templateUrl: 'usuario.html',
})
export class UsuarioPage {
  mapRoot = 'MapPage';
  destaquesRoot = 'DestaquesPage';
  novaFestaRoot = 'EditFestaPage';
  amigosRoot = 'AmigosPage';
  configRoot = 'ConfigPage';
  constructor(public sessao:SessaoProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          if(!res.pessoa_fisica) this.navCtrl.setRoot('BaladaPage');
          console.log(res);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }
  @ViewChild('userTabs') tabRef: Tabs;

ionViewDidEnter() {
  this.tabRef.select(0);
 }

}
