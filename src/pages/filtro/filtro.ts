import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ViewController } from 'ionic-angular';
import { FestaProvider } from '../../providers/festa/festa';

/**
 * Generated class for the FiltroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filtro',
  templateUrl: 'filtro.html',
})
export class FiltroPage {
  festa = {
    open:'',
    price:{lower:0,upper:1000},
    public:'',
    style_music:'',
    min_old:''
  }
  estilos = [];
  publico = [];
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    private modal:ModalController,
    private festaProvider:FestaProvider
  ) {
  }

  ionViewDidEnter() {
    console.log('ionViewDidLoad FiltroPage');
    this.estilos = this.festaProvider.estilosDeMusica();
    this.publico = this.festaProvider.publicos();
  }
  pesquisar(){
    this.viewCtrl.dismiss(this.festa);
  }
  closemodal(){
    this.navCtrl.pop();
  }

}
