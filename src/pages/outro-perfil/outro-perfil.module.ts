import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OutroPerfilPage } from './outro-perfil';

@NgModule({
  declarations: [
    OutroPerfilPage,
  ],
  imports: [
    IonicPageModule.forChild(OutroPerfilPage),
  ],
})
export class OutroPerfilPageModule {}
