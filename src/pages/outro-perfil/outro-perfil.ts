import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController } from 'ionic-angular';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider } from '../../providers/sessao/sessao';

/**
 * Generated class for the OutroPerfilPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-outro-perfil',
  templateUrl: 'outro-perfil.html',
})
export class OutroPerfilPage {
  tela:any = 'eventos';
  usuario = {_id:'',name:'',nickname:'',pessoa_fisica:true,profile_photo:"assets/imgs/avatar.png",friends:[],favorite_parties:[]};
  sucesso = {friends:false,parties:false};
  seguidor = {_id:'',pessoa_fisica:true};
  qtdSeguidores = 0;
  qtdfestas = 0;
  seguidores = [];
  festas = [];
  constructor(
    public navCtrl: NavController,
    public modalCtrl:ModalController,
    public toast:ToastController,
    public navParams: NavParams,
    public modal: ModalController,
    private usuarioProvider: UsuarioProvider,
    private festaProvider: FestaProvider,
    private sessao: SessaoProvider

  ) {
    this.sessao.get()
    .then(res => {
      if(res){
        this.seguidor = res;
        console.log('seguidor')
        console.log(this.seguidor);
      }
      else {
        alert('Usuário não logado');
        this.sessao.remove();
      }
    });
  }

  ionViewDidEnter(){
    console.log('ionViewDidLoad OutroPerfilPage');
    if(this.navParams.data.id){
      this.pesquisarUsuario(this.navParams.data.id);
    }
  }
  listarFestas(usuario_id){
    let payload = {userId:usuario_id,will_go:true}
    this.festaProvider.listarFestasComparecidas(payload)
    .subscribe(
      response=>{
        console.log('listar festas')
        console.log(JSON.parse(response._body));
        this.festas = JSON.parse(response._body);
        this.qtdfestas = this.festas.length;

      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    );
    this.sucesso.parties = true;
  }
  listarFestasCriadas(usuario_id){
    this.festaProvider.listarFestasDoUsuario(usuario_id)
    .subscribe(
      response=>{
        console.log('listar festas')
        console.log(JSON.parse(response._body));
        this.festas = JSON.parse(response._body);
        this.qtdfestas = this.festas.length;

      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    );
    this.sucesso.parties = true;
  }
  listarAmigos(usuario_id){
    this.usuarioProvider.listarAmigos(usuario_id)
    .subscribe(
      response => {
        console.log('listar amigos');
        let seguidor_id = this.seguidor._id;
        this.seguidores = JSON.parse(response._body);
        this.seguidores = this.seguidores.map(function(obj){

          let selected = obj.friends.filter(function(fr){return fr == seguidor_id});
          obj= {
            _id:obj._id,
            name:obj.name,
            selected:(selected.length > 0)?true:false,
            profile_photo:obj.profile_photo[0],
            gender:obj.gender,
            nickname:obj.nickname,
            friends:obj.friends
          }

          return obj;
        });
        this.qtdSeguidores = this.seguidores.length;
        console.log(this.seguidores);
        if(this.usuario.pessoa_fisica) this.listarFestas(this.usuario._id);
        else this.listarFestasCriadas(this.usuario._id);
      },
      error => {
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
        if(this.usuario.pessoa_fisica) this.listarFestas(this.usuario._id);
        else this.listarFestasCriadas(this.usuario._id);

      }
    );

  }
  seguir(act,userId){
    let data = {act:act,userId:userId,friendId:this.usuario._id};
    this.usuarioProvider.controlarSeguidores(data)
    .subscribe(
      response => {
        let friend = this.seguidores.filter(function(obj){
          return obj._id == userId;
        })
        friend[0].selected = !friend[0].selected;
        console.log(this.seguidores)
        this.toast.create({message:response._body,duration:3000}).present();
      },
      error => {
        alert(error._body);
      }
    )
  }
  detalharFesta(id){
    let modal = this.modal.create('DetalhesFestaPage',{id:id});
    modal.present();
    modal.onDidDismiss(
      data=>{
        if (data) {
          if(data.owner_id != this.seguidor._id) this.navCtrl.push('OutroPerfilPage',{id:data.owner_id});
            else this.navCtrl.push('PerfilPage');
        }
      }
    )
  }
  visualizarPerfil(id){
    let page = (id!=this.seguidor._id)?'OutroPerfilPage':'PerfilPage';
    let data = (id!=this.seguidor._id)?{id:id}:{};
    this.navCtrl.push(page,data);
  }
  pesquisarUsuario(id){
    this.usuarioProvider.pesquisarUsuario(id)
    .subscribe(
      response => {
        let usuario = JSON.parse(response._body);
        console.log(usuario)
        if(usuario){
          this.usuario._id = usuario._id;
          this.usuario.name = usuario.name;
          this.usuario.nickname = usuario.nickname;
          this.usuario.profile_photo = usuario.profile_photo[0];
          this.usuario.pessoa_fisica = usuario.pessoa_fisica;

          //
          this.listarAmigos(this.usuario._id);
        }
        console.log(this.usuario)
      },
      error => {
        alert(error);
      }
    )
  }
}
