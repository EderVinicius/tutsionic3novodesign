import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController, NavParams } from 'ionic-angular';
import { UsuarioProvider} from '../../providers/usuario/usuario';
import { SessaoProvider} from '../../providers/sessao/sessao';

/**
 * Generated class for the AmigosbPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-amigosb',
  templateUrl: 'amigosb.html',
})
export class AmigosbPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toast: ToastController,
    private usuarioProvider: UsuarioProvider,
    private sessao: SessaoProvider

  ) {
  }
  seguidores = [];
  ionViewWillEnter(){
    console.log('ionViewDidLoad AmigosPage');
    this.sessao.get()
    .then(
      res => {
        if(res._id){
          console.log(res)
          this.loadAmigos(res._id);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }
  visualizarPerfil(id){
    this.navCtrl.push('OutroPerfilPage',{id:id});
  }
  loadAmigos(id){
    this.usuarioProvider.listarAmigos(id)
    .subscribe(
      response => {
        console.log(response._body)
        this.seguidores = JSON.parse(response._body);
        console.log(this.seguidores)
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    )
  }

}
