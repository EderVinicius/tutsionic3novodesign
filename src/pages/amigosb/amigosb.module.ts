import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AmigosbPage } from './amigosb';

@NgModule({
  declarations: [
    AmigosbPage,
  ],
  imports: [
    IonicPageModule.forChild(AmigosbPage),
  ],
})
export class AmigosbPageModule {}
