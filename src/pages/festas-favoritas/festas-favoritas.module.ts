import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FestasFavoritasPage } from './festas-favoritas';

@NgModule({
  declarations: [
    FestasFavoritasPage,
  ],
  imports: [
    IonicPageModule.forChild(FestasFavoritasPage),
  ],
})
export class FestasFavoritasPageModule {}
