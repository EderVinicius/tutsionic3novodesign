import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController, ModalController, NavParams } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { FestaProvider } from '../../providers/festa/festa';
/**
 * Generated class for the FestasFavoritasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-festas-favoritas',
  templateUrl: 'festas-favoritas.html',
})
export class FestasFavoritasPage {
  usuario = {_id:''};
  festas = [];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public modal:ModalController,
    public toast:ToastController,
    private sessao: SessaoProvider,
    private festaProvider: FestaProvider
  ) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          this.usuario._id = res._id;
          this.listarFestas(this.usuario._id);
        } else {
          alert('Usuário não logado')
          this.sessao.remove()
        }
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FestasFavoritasPage');
  }
  listarFestas(usuario_id){let payload = {userId:usuario_id,will_go:false}
  this.festaProvider.listarFestasComparecidas(payload)
    .subscribe(
      response=>{
        console.log(response);
        this.festas = JSON.parse(response._body);
      },
      error => {
        console.log(error);
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
      }
    )
  }
  detalharFesta(id){
    let modal = this.modal.create('DetalhesFestaPage',{id:id});
    modal.present();
    modal.onDidDismiss(
      data=>{
        if (data) {
          if(data.owner_id) this.navCtrl.push('PerfilPage');
        }
      }
    )
  }
}
