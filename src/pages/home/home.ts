import { Component } from '@angular/core';
import { NavController,AlertController,ModalController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';

import { Facebook } from '@ionic-native/facebook';
import { EditContaPage } from '../../pages/edit-conta/edit-conta';
import { RecuperarSenhaPage } from '../../pages/recuperar-senha/recuperar-senha';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { Usuario } from '../../providers/usuario/model-usuario';
import { VerificarEmailPage } from '../verificar-email/verificar-email';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(
    public navCtrl: NavController,
    public alertCtrl:AlertController,
    public modalCtrl: ModalController,
    private loginProvider:LoginProvider,
    private sessao:SessaoProvider,
    private fb: Facebook) {
    this.sessao.get()
      .then(res => {
        if(res){
          console.log('Logado')
          if(res.pessoa_fisica){
            this.navCtrl.push('UsuarioPage');
          } else {
            this.navCtrl.push('BaladaPage');
          }
        } else {
          console.log('Não logado')
        }
      });
  }
  private login = {email:'',password:''};
  entrar(idfacebook=null,nome=null,foto=null){
    if(idfacebook && !this.login.email){
      let login = {idfacebook:idfacebook,password:this.login.password};
      this.loginProvider.getLoginFacebook(login)
      .subscribe(
        response=>{
          let usuario= JSON.parse(response._body);
          this.login.email = usuario.email;
          this.entrar();
        },
        error=>{
          console.log(error);
          this.salvarFacebook(this.login,idfacebook,nome,foto);
        }
      );
    }else{      
      this.loginProvider.getLogin(this.login)
      .subscribe(
        response=>{
          console.log(response);
          let user = JSON.parse(response._body)
          let model = new Usuario();
          model._id = user._id;
          model.name = user.name;
          model.cpf_cnpj = user.cpf_cnpj;
          model.pessoa_fisica = user.pessoa_fisica;
          model.dob = user.dob;
          model.gender = user.gender;
          model.email = user.email;
          model.idfacebook = user.idfacebook;
          this.sessao.create(model);

          if(user.pessoa_fisica){
            this.navCtrl.setRoot('UsuarioPage');
          } else {
            this.navCtrl.setRoot('BaladaPage');
          }
        },
        error=>{
          console.log(error);
          if(error.status == 403){
            let result = JSON.parse(error._body);
            alert(result.message);
            this.verificarEmail(this.login.email,result.id);
          }
          else if(idfacebook){
            this.salvarFacebook(this.login,idfacebook,nome,foto);
          } else {
            alert(error._body);
          }
        });
    }
  }
  salvarFacebook(usuario,idfacebook,nome,foto){
    let login = {idfacebook:'',nome:'',email:'',password:'',foto:''}
        //recebendo dados de login
        login.idfacebook = idfacebook;
        login.email = usuario.email;
        login.password = idfacebook;
        login.nome = nome;
        login.foto = foto;
        this.criarConta(login);
  }
  verificarEmail(email,id){
    this.modalCtrl.create(VerificarEmailPage,{email:email,id:id}).present().then(res=>{console.log(res)}).catch(e=>{console.log(e)});
  }
  criarConta(data = null) {
    let modal = this.modalCtrl.create(EditContaPage,data);
    modal.present();
    modal.onDidDismiss((data)=>{
      if(data){
        this.login = data;
        this.entrar();
      }
    })
  }
  //método para chamar api do facebook e salvar no banco o usuario
  loginFacebook() {
     let permissions = new Array<string>();
     permissions = ["public_profile", "email"];
     this.fb.login(permissions).then((response) => {
      let params = new Array<string>();

      this.fb.api("/me?fields=name,email,picture.type(large)", params)
      .then(res => {
        this.login.email = res.email;
        this.login.password = res.id;
        this.entrar(res.id,res.name,res.picture.data.url);
      }, (error) => {
        alert(error);
        console.log('ERRO LOGIN: ',error);
      })
    }, (error) => {

      alert(error);
    });
  }
  recuperarSenha(){
    this.modalCtrl.create(RecuperarSenhaPage).present();
  }
}
