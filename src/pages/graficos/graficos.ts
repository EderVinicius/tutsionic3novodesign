import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as HighCharts from 'highcharts';
import moment from 'moment';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider } from '../../providers/sessao/sessao';
/**
 * Generated class for the GraficosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-graficos',
  templateUrl: 'graficos.html',
})
export class GraficosPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private festaProvider: FestaProvider,
    private sessao: SessaoProvider
  ) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          console.log(res);
          this.filtroDatas.owner_id = res._id;
          this.listarNomesFesta(res._id);


        } else {
          alert('Usuário não logado');
          this.sessao.remove();
        }
      }
    );
  }
  festasNome = [];
  festaPopular = "";
  filtroDatas = {
    start:'',
    end:'',
    owner_id:''
  }
  festasDesseMes = [];
  festa = {title:'',will_go:[]};
  ionViewDidEnter() {
    console.log('ionViewDidLoad GraficosPage');
  }
  listarNomesFesta(usuario_id){
    this.festaProvider.listarNomesFestaPorUsuario(usuario_id)
    .subscribe(
      response => {
        this.festasNome = JSON.parse(response._body);
        this.pegarPopularidadeDoMesAnterior();
      },
      error => {
        alert(error._body);
        this.pegarPopularidadeDoMesAnterior();
      }
    )

  }
  pesquisarPopularidadeDaFesta(){
    console.log('kdldld')
    this.festaProvider.pesquisarPopularidadeFesta(this.festaPopular)
    .subscribe(
      response => {
        this.festa = JSON.parse(response._body);

        let resultado = [];
        let faixaEtaria = [0,0,0,0];
        let publico = [];

        this.festa.will_go.forEach(obj=>{
          console.log(obj.date)
          let data = resultado.filter(obj1=>{return obj1.data == obj.date});
          console.log(resultado)
          if(data.length == 0){

            resultado.push({data:obj.date,pessoas:0});
          }
          console.log(obj.user_id.gender)
          let genero = publico.filter(obj1=>{return obj1.name == obj.user_id.gender});
          console.log(publico)
          if(genero.length == 0){
            publico.push({name:obj.user_id.gender,y:0});
          }
          console.log(obj.user_id.dob);
          let dataA = Number(obj.user_id.dob.split('T')[0].split('-')[0]);
          let anoAtual = new Date(Date.now()).getFullYear();
          let diferenca = anoAtual - dataA;
          if(diferenca >= 25){
            faixaEtaria[0]++;
          } else if(diferenca >= 21 ){
            faixaEtaria[1]++;
          } else if(diferenca >= 18){
            faixaEtaria[2]++;
          } else if(diferenca < 18){
            faixaEtaria[3]++;
          }
        });
        resultado.forEach(obj=>{
          let retorno = this.festa.will_go.filter(obj1=>{return obj1.date == obj.data;});
          obj.pessoas = retorno.length;

        });
        publico.forEach(obj=>{
          let retorno = this.festa.will_go.filter(obj2=>{return obj2.user_id.gender == obj.name;});
          obj.y = retorno.length
        });
        console.log(publico);
        console.log(resultado);
        var pop = HighCharts.chart('pops', {
          chart: {
          type: 'area',
          spacingBottom: 30
          },
          title: {
            text: 'Popularidade da festa '+this.festa.title
          },

          xAxis: {
            allowDecimals: false,
            categories: resultado.map(obj=>{
              let dataA = obj.data.split('T')[0].split('-');
              let dataF = dataA[2]+'/'+dataA[1]+'/'+dataA[0];
              return dataF;
            })
          },
          yAxis: {
            allowDecimals: false,
            title: {
              text: 'Presenças confirmadas'
            },
            labels: {
              formatter: function () {
                  return this.value;
              }
            }
          },
          credits: {
            enabled: false
          },
          series: [{
            name: this.festa.title,
            data: resultado.map(obj=>{return obj.pessoas})
          }]
        });
        var pizza = HighCharts.chart('ps', {
          chart: {
            type: 'pie'
          },
          title: {
            text: 'Publico da festa '+this.festa.title
          },
          credits: {
            enabled: false
          },
          colors: ['#2f7ed8', '#0d233a', '#372042', '#910000', '#1aadce','#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
          series: [
            {
              name: 'Presenças confirmadas',
              data: publico
            }
          ]
        });

        var barra = HighCharts.chart('is', {
          chart: {
            type: 'bar'
          },
          title: {
            text: 'Faixa de idade na festa '+this.festa.title
          },
          credits: {
            enabled: false
          },
          colors: ['#2f7ed8', '#0d233a', '#372042', '#910000', '#1aadce','#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
          xAxis: {
            allowDecimals: false,
            categories: ['Acima de 25 anos', 'Entre 21 e 25 anos', 'Entre 18 e 21 anos', 'Menor de 18']
          },
          yAxis: {
            allowDecimals: false,
            title: {
              text: 'Presenças confirmadas'
            }
          },
          series: [
            {
              name: 'Presenças confirmadas',
              data: faixaEtaria
            }
          ]
        });
      },
      error => {

      }
    )
  }
  pesquisarFestasEntreDatas(){
    this.festaProvider.listarFestasDesseMes(this.filtroDatas)
    .subscribe(
      response => {
        this.festasDesseMes = JSON.parse(response._body);
        let publicoGeral = [];
        let publico = ['Masculino','Feminino','Outros'];
        let faixaEtaria = [
          {name:'Acima de 25 anos', y:0},
          {name:'Entre 21 e 25 anos',y:0},
          {name:'Entre 18 e 21 anos',y:0},
          {name:'Menor de 18',y:0}
        ];
        this.festasDesseMes.forEach(
          festa => {
            console.log(festa);
            let quantidade = [0,0,0];
            festa.will_go.forEach(
              user => {
                switch(user.user_id.gender){
                  case publico[0]:
                    quantidade[0]++;
                  break;
                  case publico[1]:
                    quantidade[1]++;
                  break;
                  case publico[2]:
                    quantidade[2]++;
                  break;
                }
                console.log(user.user_id.dob);
                let dataA = Number(user.user_id.dob.split('T')[0].split('-')[0]);
                let anoAtual = new Date(Date.now()).getFullYear();
                let diferenca = anoAtual - dataA;
                if(diferenca >= 25){
                  faixaEtaria[0].y++;
                } else if(diferenca >= 21 ){
                  faixaEtaria[1].y++;
                } else if(diferenca >= 18){
                  faixaEtaria[2].y++;
                } else if(diferenca < 18){
                  faixaEtaria[3].y++;
                }
              }
            )


            publicoGeral.push({
              name:festa.title,
              data:quantidade
            })
          }
        )
        let start = this.filtroDatas.start.split('T')[0].split('-');
        let end = this.filtroDatas.end.split('T')[0].split('-');
        let intervalo = (this.filtroDatas.start && this.filtroDatas.end)?start[2]+'/'+start[1]+'/'+start[0]+' e '+ end[2]+'/'+end[1]+'/'+end[0]:'os ultimos 30 dias.';

        var geral = HighCharts.chart('ig', {
          chart: {
            type: 'pie'
          },
          title: {
            text: 'Faixa etária geral'
          },
          credits: {
            enabled: false
          },
          colors: ['#2f7ed8', '#0d233a', '#372042', '#910000', '#1aadce','#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'],
          series: [
            {
              name: 'Presenças confimadas',
              data: faixaEtaria
            }
          ]
        });

        var popularidade = HighCharts.chart('pg', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Publico Geral'
          },
          credits: {
            enabled: false
          },
          xAxis: {
            allowDecimals: false,
            title: {
              text: 'Eventos que ocorrem entre '+intervalo
            },
              categories:['Masculino','Feminino','Outro']
          },
          yAxis: {
              allowDecimals: false,
              title: {
                  text: 'Presenças confirmadas'
              }
          },
          series: publicoGeral
        });
        ///////////////////////////////////////////////////////////
        
        HighCharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
        backgroundColor: {
            linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
            stops: [
                [0, '#2a2a2b'],
                [1, '#3e3e40']
            ]
        },
        style: {
            fontFamily: '\'Unica One\', sans-serif'
        },
        plotBorderColor: '#606063'
    },
    title: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase',
            fontSize: '20px'
        }
    },
    subtitle: {
        style: {
            color: '#E0E0E3',
            textTransform: 'uppercase'
        }
    },
    xAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        title: {
            style: {
                color: '#A0A0A3'
  
            }
        }
    },
    yAxis: {
        gridLineColor: '#707073',
        labels: {
            style: {
                color: '#E0E0E3'
            }
        },
        lineColor: '#707073',
        minorGridLineColor: '#505053',
        tickColor: '#707073',
        tickWidth: 1,
        title: {
            style: {
                color: '#A0A0A3'
            }
        }
    },
    tooltip: {
        backgroundColor: 'rgba(0, 0, 0, 0.85)',
        style: {
            color: '#F0F0F0'
        }
    },
    plotOptions: {
        series: {
            dataLabels: {
                color: '#B0B0B3'
            },
            marker: {
                lineColor: '#333'
            }
        },
        boxplot: {
            fillColor: '#505053'
        },
        candlestick: {
            lineColor: 'white'
        },
        errorbar: {
            color: 'white'
        }
    },
    legend: {
        itemStyle: {
            color: '#E0E0E3'
        },
        itemHoverStyle: {
            color: '#FFF'
        },
        itemHiddenStyle: {
            color: '#606063'
        }
    },
    credits: {
        style: {
            color: '#666'
        }
    },
    labels: {
        style: {
            color: '#707073'
        }
    },
  
    drilldown: {
        activeAxisLabelStyle: {
            color: '#F0F0F3'
        },
        activeDataLabelStyle: {
            color: '#F0F0F3'
        }
    },
  
    navigation: {
        buttonOptions: {
            symbolStroke: '#DDDDDD',
            theme: {
                fill: '#505053'
            }
        }
    },
  
    // scroll charts
    rangeSelector: {
        buttonTheme: {
            fill: '#505053',
            stroke: '#000000',
            style: {
                color: '#CCC'
            },
            states: {
                hover: {
                    fill: '#707073',
                    stroke: '#000000',
                    style: {
                        color: 'white'
                    }
                },
                select: {
                    fill: '#000003',
                    stroke: '#000000',
                    style: {
                        color: 'white'
                    }
                }
            }
        },
        inputBoxBorderColor: '#505053',
        inputStyle: {
            backgroundColor: '#333',
            color: 'silver'
        },
        labelStyle: {
            color: 'silver'
        }
    },
  
    navigator: {
        handles: {
            backgroundColor: '#666',
            borderColor: '#AAA'
        },
        outlineColor: '#CCC',
        maskFill: 'rgba(255,255,255,0.1)',
        series: {
            color: '#7798BF',
            lineColor: '#A6C7ED'
        },
        xAxis: {
            gridLineColor: '#505053'
        }
    },
  
    scrollbar: {
        barBackgroundColor: '#808083',
        barBorderColor: '#808083',
        buttonArrowColor: '#CCC',
        buttonBackgroundColor: '#606063',
        buttonBorderColor: '#606063',
        rifleColor: '#FFF',
        trackBackgroundColor: '#404043',
        trackBorderColor: '#404043'
    },
  
    // special colors for some of the
    legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
    background2: '#505053',
    dataLabelsColor: '#B0B0B3',
    textColor: '#C0C0C0',
    contrastTextColor: '#F0F0F3',
    maskColor: 'rgba(255,255,255,0.3)'
  };
  
  // Apply the theme
  popularidade.setOptions(HighCharts.theme);
        ///////////////////////////////////////////////////////////
      },
      error => {
        alert(error._body)
      }
    )
  }
  pegarPopularidadeDoMesAnterior(){
    let dataAtual = moment().subtract(3,"hour").format('YYYY-MM-DD');
    let dataAnterior = moment(dataAtual).subtract(1,"month").format('YYYY-MM-DD');
    let dataDoisMesesAtras = moment(dataAnterior).subtract(1,"month").format('YYYY-MM-DD');

    this.filtroDatas.start = dataDoisMesesAtras;
    this.filtroDatas.end = dataAnterior;
    let resultados = [0,0];
    this.festaProvider.listarFestasDesseMes(this.filtroDatas)
    .subscribe(
      response=>{
        let festas = JSON.parse(response._body);
        console.log(festas)
        festas.forEach(
          f=>{
            resultados[0] += f.will_go.length;
            console.log('Mes anterior'+f.will_go.length);
          }

        );
        this.filtroDatas.start = dataAnterior;
        this.filtroDatas.end = dataAtual;
        let dataAtualF = dataAtual.split('T')[0].split('-')
        let dataAnteriorF = dataAnterior.split('T')[0].split('-')
        let intervalo = dataAnteriorF[2] + '/'+ dataAnteriorF[1] +'/'+ dataAnteriorF[0]+ ' e '+ dataAtualF[2] + '/'+ dataAtualF[1] +'/'+ dataAtualF[0]
        this.festaProvider.listarFestasDesseMes(this.filtroDatas)
        .subscribe(
          response=>{
            let festas1 = JSON.parse(response._body);
            console.log(festas1)
            festas1.forEach(
              f=>{
                resultados[1] += f.will_go.length;
                console.log('esse mes'+resultados[1]);
              }
            );
            var popg = HighCharts.chart('popg', {
              chart: {
              type: 'area',
              spacingBottom: 30
              },
              title: {
                text: 'Popularidade Geral '
              },

              xAxis: {
                allowDecimals: false,
                categories: ['Mês anterior','Mês atual'],
                title: {
                  text: 'Eventos que ocorrem entre '+intervalo
                },
              },
              yAxis: {
                allowDecimals: false,
                title: {
                  text: 'Presenças confirmadas'
                },
                labels: {
                  formatter: function () {
                      return this.value;
                  }
                }
              },
              credits: {
                enabled: false
              },
              series: [{
                name: 'Presenças confirmadas',
                data: resultados
              }]
            });
            this.pesquisarFestasEntreDatas();
          },
          error=>{
            alert(error._body)
            this.pesquisarFestasEntreDatas();
          }
        );
      },
      error=>{
        alert(error._body)
        this.pesquisarFestasEntreDatas();
      }
    );
  }
}
