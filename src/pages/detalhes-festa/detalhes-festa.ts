import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, ToastController } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { FestaProvider } from '../../providers/festa/festa';
import { UsuarioProvider } from '../../providers/usuario/usuario';
/**
 * Generated class for the DetalhesFestaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalhes-festa',
  templateUrl: 'detalhes-festa.html',
})
export class DetalhesFestaPage {
  usuario = {_id:'',pessoa_fisica:true};
  selected = false;
  go = false;
  festa = {
    _id:'',
    owner_id: {
      _id:'',
      nickname:'',
      name:'',
      profile_photo:'',
      friends:[]
    },
    will_go:[],
    title:"Festa não encontrada",
    description:"Festa não encontrada",
    date:'',
    picture:[
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png',
      'assets/imgs/IconeFestas.png'
    ],
    time:{
      open:'',
      close:''
    },
    formatted_address:'',
    address:{
      cep:'',
      street:'',
      number:'',
      city:'',
      state:'',
      neighborhood:''
    },
    style_music:'Nada',
    style_music_array:[],
    public:'Nenhum',
    price_male:0,
    price_female:0,
    min_old:18
  }
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public toast: ToastController,
    public navParams: NavParams,
    private sessao: SessaoProvider,
    private festaProvider:FestaProvider,
    private usuarioProvider:UsuarioProvider
  ) {
    this.loadUsuario();
  }

  ionViewWillEnter(){
    console.log('ionViewDidLoad DetalhesFestaPage');
    this.loadUsuario();
  }
  loadUsuario(){
    this.sessao.get()
    .then(
      res=>{
        if(res){
          this.usuario = res;
          this.usuario._id = res._id;
          this.usuario.pessoa_fisica = res.pessoa_fisica;
          console.log(this.usuario);
          if(this.navParams.data.id){
            this.loadFesta();
          }
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }

      }
    );
  }
  loadFesta(){
    this.festaProvider.buscarFesta(this.navParams.data.id)
    .subscribe(
      response=>{
        console.log(response);
        let f = JSON.parse(response._body);
        this.festa = f;
        //this.festa.style_music.concat(f.style_music);
        this.festa.style_music = f.style_music;
        this.festa.style_music_array = f.style_music;
        this.festa.owner_id.profile_photo = (this.festa.owner_id.profile_photo.length > 0)?this.festa.owner_id.profile_photo[0]:'';
        let seguidorId = this.usuario._id
        let seguidores = this.festa.owner_id.friends.filter(function(obj){return obj == seguidorId;});
        let festasComparecidas = this.festa.will_go.filter(function(obj){return obj.user_id == seguidorId});
        this.selected = (seguidores.length >0);
        this.go = (festasComparecidas.length > 0);
        console.log('Criador da festa: '+this.festa.owner_id.name);
        console.log(this.festa);

      },
      error=>{
        console.log(error)
        alert(error._body);
      }
    );
  }
  closemodal(){
    this.navCtrl.pop();
  }
  visualizarPerfil(id){

    let data = {owner_id:id};
    this.viewCtrl.dismiss(data);
  }
  seguir(act,userId){
    let data = {act:act,userId:userId,friendId:this.usuario._id};
    this.usuarioProvider.controlarSeguidores(data)
    .subscribe(
      response => {

        this.selected = !this.selected;

        this.toast.create({message:response._body,duration:3000}).present();
      },
      error => {
        alert(error._body);
      }
    )
  }
  participar(act){
    let data = {act:act,partyId:this.festa._id,userId:this.usuario._id};
    this.festaProvider.participarFesta(data)
    .subscribe(
      response => {
        if(act == 'ADD') {
          this.go = true;
          this.toast.create({message:'Marcado como vou',duration:3000}).present();
        }
        else {
          this.go = false;
          this.toast.create({message:'Marcado como não vou',duration:3000}).present();
        }
      },
      error => {
        console.log(error._body);
        alert(error._body);
      }
    );
  }
}
