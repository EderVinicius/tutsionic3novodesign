import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { ViacepService, Endereco, CepError } from '@brunoc/ngx-viacep';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { ViewController } from 'ionic-angular/navigation/view-controller';
/**
 * Generated class for the EstacionamentoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-estacionamento',
  templateUrl: 'estacionamento.html',
})
export class EstacionamentoPage {
  editEstacionamento:any;
  safe = false;
  parking = {
    safe:'Não',
    price: '',
    title: '',
    address: {
        street: '',
        number: '',
        cep: '',
        city: '',
        state: '',
        neighborhood: ''
    },
    description:''
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viacep: ViacepService,
    public viewCtrl: ViewController,
    private modal: ModalController,
    public toast: ToastController,
    private formBuilder: FormBuilder
  ) {
    this.editEstacionamento = this.formBuilder.group({
      titulo:[this.parking.title,Validators.required],
      seguro:[this.safe],
      cep:[this.parking.address.cep,Validators.required],
      rua:[this.parking.address.street,Validators.required],
      numero:[this.parking.address.number,Validators.required],
      bairro:[this.parking.address.neighborhood,Validators.required],
      cidade:[this.parking.address.city,Validators.required],
      estado:[this.parking.address.state,Validators.required],
      preco:[this.parking.price,Validators.required],
      descricao:[this.parking.description,Validators.required]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EstacionamentoPage');
  }
  completarEndereco(){
    this.viacep.buscarPorCep(this.parking.address.cep)
    .then( ( endereco: Endereco ) => {
      // Endereço retornado :)
      this.parking.address.street = endereco.logradouro;
      this.parking.address.city = endereco.localidade;
      this.parking.address.state = endereco.uf;
      this.parking.address.neighborhood = endereco.bairro;
      console.log(endereco);
    }).catch( (error: CepError) => {
      // Alguma coisa deu errado :/
      alert(error.descricao);
      console.log(error.descricao);
    });
  }
  salvar(){
    let data = this.parking;
    this.viewCtrl.dismiss(data);
  }
  closemodal(){
    this.navCtrl.pop();
  }
  seguro(safe){
    if(safe){
      this.parking.safe = 'Sim';
    } else {
      this.parking.safe = 'Não';
    }
  }
}
