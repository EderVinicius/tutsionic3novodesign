import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Tabs } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';

/**
 * Generated class for the BaladaPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-balada',
  templateUrl: 'balada.html'
})
export class BaladaPage {

  graficosRoot = 'GraficosPage'
  novaFestabRoot = 'NovaFestabPage'
  configbRoot = 'ConfigbPage'


  constructor(public navCtrl: NavController,public sessao:SessaoProvider) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          if(res.pessoa_fisica) this.navCtrl.setRoot('UsuarioPage');
          console.log(res);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }
  @ViewChild('companyTabs') tabRef: Tabs;

  ionViewDidEnter() {
    this.tabRef.select(0);
   }
}
