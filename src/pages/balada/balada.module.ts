import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BaladaPage } from './balada';

@NgModule({
  declarations: [
    BaladaPage,
  ],
  imports: [
    IonicPageModule.forChild(BaladaPage),
  ]
})
export class BaladaPageModule {}
