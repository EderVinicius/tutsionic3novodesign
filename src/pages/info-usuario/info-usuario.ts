import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';

import { FormBuilder, Validators } from '@angular/forms';

import { UsuarioProvider } from '../../providers/usuario/usuario';

import { SessaoProvider } from '../../providers/sessao/sessao';

import { Usuario } from '../../providers/usuario/model-usuario';
import { HomePage } from '../home/home';
/**
 * Generated class for the InfoUsuarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-info-usuario',
  templateUrl: 'info-usuario.html',
})
export class InfoUsuarioPage {
  usuarioLogado: any;
  email_old:'';
  usuario = { _id: '', idfacebook: '', nome: '', nickname: '', email: '', password: '', confpassword: '', pessoaFisica: true, pessoaJuridica: false, cnpj: '', dataNasc: '', sexo: '', celular: '', descricao: '' };
  editar = false;
  public editUsuario: any;
  pessoaJuridica = false;
  message = {
    nome: '',
    nickname: '',
    celular: '',
    email: '',
    password: '',
    cnpj: '',
    dataNasc: '',
    sexo: ''
  }
  constructor(
    public navCtrl: NavController,
    public toast: ToastController,
    public alertCtrl: AlertController,
    private usuarioProvider: UsuarioProvider,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public sessao: SessaoProvider

  ) {
    this.formEmpresa(false);
    this.usuarioLogado = new Usuario();
    this.sessao.get()
      .then(res => {
        if(res){
          this.usuarioLogado = res;
          this.init(res._id);

          console.log(res);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }

      });



  }
  init(id) {
    this.usuarioProvider.pesquisarUsuario(id)
      .subscribe(
      response => {

        this.usuarioLogado = JSON.parse(response._body);
        console.log(id);
        if (this.usuarioLogado._id) {
          this.usuario._id = this.usuarioLogado._id;
          this.usuario.idfacebook = this.usuarioLogado.idfacebook;
          this.usuario.nome = this.usuarioLogado.name;
          this.usuario.nickname = this.usuarioLogado.nickname;
          this.usuario.celular = this.usuarioLogado.celphone;
          this.usuario.email = this.usuarioLogado.email;
          this.email_old = this.usuarioLogado.email;
          this.usuario.password = this.usuarioLogado.password;
          this.usuario.cnpj = this.usuarioLogado.cpf_cnpj;
          this.usuario.pessoaFisica = this.usuarioLogado.pessoa_fisica;
          this.usuario.pessoaJuridica = !this.usuario.pessoaFisica;
          this.usuario.sexo = this.usuarioLogado.gender;
          this.usuario.dataNasc = this.usuarioLogado.dob;
          this.usuario.confpassword = (this.usuarioLogado.idfacebook) ? this.usuario.password : '';
          this.pessoaJuridica = this.usuario.pessoaJuridica;
          this.formEmpresa(this.usuario.pessoaJuridica);

        }

      },
      err => {
        console.log(err)
      }
      )
  }
  formEmpresa(pessoaJuridica) {
    this.usuario.pessoaJuridica = pessoaJuridica;
    if (pessoaJuridica) {
      this.editUsuario = this.formBuilder.group({
        pessoaJuridica: [true, Validators.required],
        nome: [this.usuario.nome, Validators.required],
        nickname: [this.usuario.nickname, Validators.required],
        cnpj: [this.usuario.cnpj, Validators.required],
        celular: [this.usuario.celular, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
        email: [this.usuario.email, Validators.compose([Validators.email, Validators.required])],
        password: [this.usuario.password, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
        confpassword: [this.usuario.confpassword, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])]
      });
    } else {
      this.editUsuario = this.formBuilder.group({
        pessoaJuridica: [false, Validators.required],
        nome: [this.usuario.nome, Validators.required],
        nickname: [this.usuario.nickname, Validators.required],
        celular: [this.usuario.celular, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
        dataNasc: [this.usuario.dataNasc, Validators.required],
        sexo: [this.usuario.sexo, Validators.required],
        email: [this.usuario.email, Validators.compose([Validators.email, Validators.required])],
        password: [this.usuario.password, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])],
        confpassword: [this.usuario.confpassword, Validators.compose([Validators.minLength(6), Validators.maxLength(20), Validators.required])]
      });
    }



  }

  editarConta() {    
    let usuario;
    this.usuario.pessoaFisica = !this.usuario.pessoaJuridica;
    let pass = true;

    if(this.usuario.pessoaJuridica && !this.validarCNPJ(this.usuario.cnpj)){
        pass = false;
    }
    

    if (this.usuario.pessoaFisica) {
      usuario = {
        _id: this.usuario._id,
        idfacebook: this.usuario.idfacebook,
        name: this.usuario.nome,
        nickname: this.usuario.nickname,
        celphone: this.usuario.celular,
        email: this.usuario.email,
        password: this.usuario.password,
        dob: this.usuario.dataNasc,
        gender: this.usuario.sexo,
        pessoa_fisica: this.usuario.pessoaFisica,
        email_confirmed:(this.email_old == this.usuario.email)
      }
    } else {
      usuario = {
        _id: this.usuario._id,
        idfacebook: this.usuario.idfacebook,
        name: this.usuario.nome,
        nickname: this.usuario.nickname,
        celphone: this.usuario.celular,
        email: this.usuario.email,
        password: this.usuario.password,
        cpf_cnpj: this.usuario.cnpj,
        pessoa_fisica: this.usuario.pessoaFisica,
        email_confirmed:(this.email_old == this.usuario.email)
      }
    }
    if(pass){
      
    if (this.usuario.pessoaJuridica != this.pessoaJuridica || this.email_old != this.usuario.email) {
      let confirm = this.alertCtrl.create({
        title: 'Deseja mudar seu estado de pessoa jurídica ou o seu e-mail?',
        message: 'Essas ações farão você sair do aplicativo. E então poderá autenticar-se novamente. Tem certeza que deseja isso?',
        buttons: [
          {
            text: 'Cancelar',
            handler: () => {
              console.log('Cancelar');
            }
          },
          {
            text: 'Ok',
            handler: () => {
              console.log('Ok');
              console.log(usuario);
              this.usuarioProvider.editarUsuario(usuario._id, usuario)
                .subscribe(
                response => {
                  //this.entrar();
                  this.init(usuario._id);
                  this.usuario.confpassword = "";
                  this.editar = false;
                  this.toast.create({ message: 'Alterações salvas', duration: 3000 }).present();
                  console.log('Alterações salvas');
                  this.logout();
                },
                error => {
                  alert(error);
                  console.log(error);
                }
                );

            }
          }
        ]
      });
      confirm.present();
    } else {
      console.log(usuario);
      this.usuarioProvider.editarUsuario(usuario._id, usuario)
        .subscribe(
        response => {
          //this.entrar();
          this.init(usuario._id);
          this.usuario.confpassword = "";
          this.editar = false;
          this.toast.create({ message: 'Alterações salvas', duration: 3000 }).present();

        },
        error => {
          alert(error);
          console.log(error);
        }
        );
    }
    }else{
      this.toast.create({ message: 'CNPJ INVÁLIDO', duration: 3000 }).present();
    }
  }
  logout(){
    console.log('logout');
    this.sessao.remove();

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoUsuarioPage');
  }

  
  //EDER
  validarCNPJ(cnpj) {
    console.log('validarCNPJ(cnpj):', cnpj);
    if(cnpj!=undefined){    
    cnpj = cnpj.replace(/[^\d]+/g,'');
    cnpj = cnpj.replace(/\D/g,'');
    var numeros, digitos, soma, i, resultado, pos, tamanho, digitos_iguais;
    digitos_iguais = 1;
    if (cnpj.length < 14 && cnpj.length < 15)
        return false;
    for (i = 0; i < cnpj.length - 1; i++)
        if (cnpj.charAt(i) != cnpj.charAt(i + 1))
    {
        digitos_iguais = 0;
        break;
    }
    if (!digitos_iguais)
    {
        tamanho = cnpj.length - 2
        numeros = cnpj.substring(0,tamanho);
        digitos = cnpj.substring(tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--)
        {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        tamanho = tamanho + 1;
        numeros = cnpj.substring(0,tamanho);
        soma = 0;
        pos = tamanho - 7;
        for (i = tamanho; i >= 1; i--)
        {
            soma += numeros.charAt(tamanho - i) * pos--;
            if (pos < 2)
                pos = 9;
        }
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
    }else{
      return false;      
    }
  }
  //EDER

}
