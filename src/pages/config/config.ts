import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { SessaoProvider } from '../../providers/sessao/sessao';
import { HomePage } from '../home/home';
/**
 * Generated class for the ConfigPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-config',
  templateUrl: 'config.html',
})
export class ConfigPage {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public sessao:SessaoProvider,

  ) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          if(!res.pessoa_fisica)this.navCtrl.push('ConfigbPage')
          console.log(res);
        } else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfigPage');
  }
  abrir(pagina){
    this.navCtrl.push(pagina);
  }
  logout(){
    this.sessao.remove();
    //this.viewCtrl.dismiss();

  }
}
