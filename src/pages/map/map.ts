import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, ToastController, LoadingController  } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { FestaProvider } from '../../providers/festa/festa';
import { SessaoProvider } from '../../providers/sessao/sessao';
/**
 * Generated class for the MapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  usuario = {_id:''};
  map: any;
  festas: any = [];
  festasF:any = [];
  myPosition = {lat:0,lng:0};
  mapOptions = {
    center: this.myPosition,
    zoom: 16,
    disableDefaultUI: true,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
      {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
      {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
      {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
      {
        featureType: 'administrative.locality',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
      },
      {
        featureType: 'poi',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
      },
      {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{color: '#263c3f'}]
      },
      {
        featureType: 'poi.park',
        elementType: 'labels.text.fill',
        stylers: [{color: '#6b9a76'}]
      },
      {
        featureType: 'road',
        elementType: 'geometry',
        stylers: [{color: '#38414e'}]
      },
      {
        featureType: 'road',
        elementType: 'geometry.stroke',
        stylers: [{color: '#212a37'}]
      },
      {
        featureType: 'road',
        elementType: 'labels.text.fill',
        stylers: [{color: '#9ca5b3'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry',
        stylers: [{color: '#746855'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{color: '#1f2835'}]
      },
      {
        featureType: 'road.highway',
        elementType: 'labels.text.fill',
        stylers: [{color: '#f3d19c'}]
      },
      {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{color: '#2f3948'}]
      },
      {
        featureType: 'transit.station',
        elementType: 'labels.text.fill',
        stylers: [{color: '#d59563'}]
      },
      {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{color: '#17263c'}]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.fill',
        stylers: [{color: '#515c6d'}]
      },
      {
        featureType: 'water',
        elementType: 'labels.text.stroke',
        stylers: [{color: '#17263c'}]
      }
    ]
  };
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation:Geolocation,
    public modal:ModalController,
    public loadingCtrl: LoadingController,
    private festaProvider:FestaProvider,
    private sessao:SessaoProvider,
    private toast:ToastController
  ) {
    this.sessao.get()
    .then(
      res => {
        if(res){
          this.usuario = res;
          console.log(this.usuario);
        }else {
          alert('Usuário não logado')
          this.sessao.remove();
        }
      }
    );
  }
  loadMap(lat,lng){

    console.log('MAP')
    this.myPosition = {lat:lat,lng:lng};
    console.log(this.myPosition);
    this.mapOptions.center = this.myPosition;

    let element:HTMLElement = document.getElementById('map');
    this.map = new google.maps.Map(element, this.mapOptions);
    const myMarkerOptions = {
      position:this.myPosition,
      map: this.map,
      icon:'assets/imgs/me.png'
    }
    const myMarker = new google.maps.Marker(myMarkerOptions);
    let toast = this.toast.create({message:'Esse é você',duration:3000,position:'middle'});

    google.maps.event.addListener(myMarker, 'click', function(){
      toast.present().then(()=>console.log('Esse é você')).catch(e=>console.log(e));
    });
    this.listarFestas();


  }
  listarFestas(){
    this.festaProvider.listarFestas()
    .subscribe(
      response=>{
        this.festas = JSON.parse(response._body);
        this.festasF = this.festas;
        console.log(this.festas);
        for(let f in this.festasF){
          let genero_musical = this.festasF[f].style_music[0].toLowerCase();
          let id = this.festasF[f]._id;
          let position = {lat:this.festasF[f].geometry.location.lat,lng:this.festasF[f].geometry.location.lng};
          let markerOptions = {
            position:position,
            map: this.map,
            //icon:'assets/imgs/musical.png'
            icon:'assets/imgs/music/map/music-'+genero_musical+'.png'
          }
          let marker = new google.maps.Marker(markerOptions);
          let modal = this.modal.create('DetalhesFestaPage',{id:id});
          modal.onDidDismiss(
            data=>{
              if (data){
                if(data.owner_id != this.usuario._id) this.navCtrl.push('OutroPerfilPage',{id:data.owner_id});
                else this.navCtrl.push('PerfilPage');
              }
            }
          );
          google.maps.event.addListener(marker, 'click', function(){
            modal.present();
          });
        }
      },
      error=>{
        this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
        console.log(error);
      }
    )
  }
  ionViewDidEnter() {
    console.log('ionViewDidLoad MapPage');
    //this.loadMap(-15.832460, -48.109909);

    let loading = this.loadingCtrl.create({
      content: 'Carregando sua localização...'
    });

    loading.present();

    this.geolocation.getCurrentPosition()
    .then((resp)=>{
      if(resp.coords.latitude && resp.coords.longitude){
        this.loadMap(resp.coords.latitude,resp.coords.longitude);
      } else {
        alert('Erro ao carregar sua localização, ligue o gps do seu dipositívo.');
      }
      loading.dismiss();
    })
    .catch((e)=>{
      loading.dismiss();
      alert('Erro ao carregar sua localização, ligue o gps do seu dipositívo.')
      console.log(e);
    });
  }

  filtrar(){
    let modal = this.modal.create('FiltroPage');
    modal.present();
    modal.onDidDismiss((data)=>{
      if(data){
        console.log(data);
        this.festaProvider.filtrarFesta(data)
        .subscribe(
          response => {
            console.log(response);
            this.festasF = JSON.parse(response._body);
            let loading = this.loadingCtrl.create({
              content: 'Carregando sua localização...'
            });

            this.geolocation.getCurrentPosition()
            .then((resp)=>{

              let lat = resp.coords.latitude;
              let lng = resp.coords.longitude;
              console.log('MAP')
              if(lat && lng){
                this.myPosition = {lat:lat,lng:lng};
                console.log(this.myPosition);
                this.mapOptions.center = this.myPosition;

                let element:HTMLElement = document.getElementById('map');
                this.map = new google.maps.Map(element, this.mapOptions);
                const myMarkerOptions = {
                  position:this.myPosition,
                  map: this.map,
                  icon:'assets/imgs/me.png'
                }
                const myMarker = new google.maps.Marker(myMarkerOptions);
                let toast = this.toast.create({message:'Esse é você',duration:3000,position:'middle'});

                google.maps.event.addListener(myMarker, 'click', function(){
                  toast.present().then(()=>console.log('Esse é você')).catch(e=>console.log(e));
                });
                console.log(this.festasF);
                for(let f in this.festasF){
                  let genero_musical = this.festasF[f].style_music[0].toLowerCase();

                  let id = this.festasF[f]._id;
                  let position = {lat:this.festasF[f].geometry.location.lat,lng:this.festasF[f].geometry.location.lng};
                  let markerOptions = {
                    position:position,
                    map: this.map,
                    icon:'assets/imgs/music/map/music-'+genero_musical+'.png'
                  }
                  let marker = new google.maps.Marker(markerOptions);
                  let modal = this.modal.create('DetalhesFestaPage',{id:id});
                  modal.onDidDismiss(
                    data=>{
                      if (data){
                        if(data.owner_id != this.usuario._id) this.navCtrl.push('OutroPerfilPage',{id:data.owner_id});
                        else this.navCtrl.push('PerfilPage');
                      }
                    }
                  );
                  google.maps.event.addListener(marker, 'click', function(){
                    modal.present();
                  });
                }
              } else {
                alert('Erro ao carregar sua localização, ligue o gps do seu dipositívo.')
              }
              loading.dismiss();
            })
            .catch((e)=>{
              loading.dismiss();
              alert('Erro ao carregar sua localização, ligue o gps do seu dipositívo.')
              console.log(e);
            });
          },
          error => {
            console.log(error)
            this.toast.create({message:error._body,duration:3000,position:'middle'}).present();
          }
        )

      }
    });

  }
}
