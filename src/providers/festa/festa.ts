import { Http, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../api/api';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the FestaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FestaProvider {

  constructor(public http: Http, private apiProvider:ApiProvider) {

    console.log('Hello FestaProvider Provider');
  }

  public salvarFesta(festa):Observable<any>{
    let headers = new Headers();
    headers.append('ident', btoa(festa.owner_id));
    let opts = new RequestOptions();
    opts.headers = headers;
    console.log(festa);
    return this.http.post(this.apiProvider.getUrl()+'/api/parties/v1',festa,opts);
  }
  public buscarFesta(festa_id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1/'+festa_id);
  }
  public filtrarFesta(festa):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/parties/v1/filter',festa);
  }
  public editarFesta(festa):Observable<any>{
    let headers = new Headers();
    headers.append('ident', btoa(festa.owner_id));
    let opts = new RequestOptions();
    opts.headers = headers;
    return this.http.put(this.apiProvider.getUrl()+'/api/parties/v1/'+festa._id,festa);
  }
  public deletarFesta(festa_id):Observable<any>{
    return this.http.delete(this.apiProvider.getUrl()+'/api/parties/v1/'+festa_id);
  }
  public pesquisarPopularidadeFesta(festa_id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1/willGo/'+festa_id);
  }
  public listarNomesFestaPorUsuario(usuario_id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1/user/'+usuario_id+'/name');
  }
  public participarFesta(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/user/party/update/v1',data);
  }

  public listarFestasComparecidas(payload):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/user/parties/willgo/v1',payload);
  }

  public listarFestas():Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1');
  }
  public listarFestasDestaques(cidade):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1/company/'+cidade);
  }
  public listarFestasDoUsuario(usuario_id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+'/api/parties/v1/user/'+usuario_id);
  }
  public listarFestasDesseMes(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/parties/v1/date',data);
  }
  public buscarPorEndereco(endereco){
    return this.http.get('https://maps.google.com/maps/api/geocode/json?key=AIzaSyBU86l0GJx6WieDxF4MZ9TIXpw15noLzuU&address='+endereco+'&sensor=false');
  }
  public buscarPorCordenadas(lat,lng){
    return this.http.get('https://maps.google.com/maps/api/geocode/json?key=AIzaSyBU86l0GJx6WieDxF4MZ9TIXpw15noLzuU&latlng='+lat+','+lng+'&sensor=false');
  }
  public estilosDeMusica(){

      return [
        {id: 1, name: "Rock", selected: false},
        {id: 2, name: "Samba", selected: false},
        {id: 3, name: "MPB", selected: false},
        {id: 4, name: "Funk", selected: false},
        {id: 5, name: "Blues", selected: false},
        {id: 6, name: "Pagode", selected: false},
        {id: 7, name: "Eletrônico", selected: false},
        {id: 8, name: "RAP", selected: false},
        {id: 9, name: "House", selected: false},
        {id: 10, name: "Outros", selected: false}
      ];

  }
  public publicos(){
    return [
      {name:"Hetero"},
      {name:"LGBT"},
      {name:"Todos"}
    ]
  }
}

