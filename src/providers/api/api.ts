
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
  //private urlApi:String = 'http://webtuts.nodejs7601.kinghost.net';
  //private urlApi:String = 'http://localhost:3000';
  private urlApi:String = 'https://apituts.herokuapp.com';
  constructor() {
    console.log('Hello ApiProvider Provider');
  }
  public getUrl():String{
    return this.urlApi;
  }
}
