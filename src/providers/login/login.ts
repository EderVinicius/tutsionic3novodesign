
import { Injectable } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

  constructor(public http: Http, private apiProvider:ApiProvider) {
    console.log('Hello LoginProvider Provider');
  }
  public getLogin(login):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/login/v1',login)

  }
  public getLoginFacebook(login):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/login-facebook/v1',login);
  }
}
