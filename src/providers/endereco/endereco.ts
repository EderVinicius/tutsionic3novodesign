import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the EnderecoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class EnderecoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello EnderecoProvider Provider');
  }
  public buscarPorCep(cep){
    return this.http.get('https://maps.google.com/maps/api/geocode/json?key=AIzaSyBU86l0GJx6WieDxF4MZ9TIXpw15noLzuU&address='+cep+'&sensor=false')
  }
}
