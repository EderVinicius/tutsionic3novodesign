import { Storage } from "@ionic/storage";

//pacote para transformar nossa classe em injetável
import { Injectable } from '@angular/core';

//import do nosso model usuario.ts
import { Usuario } from '../usuario/model-usuario';
import { HomePage } from "../../pages/home/home";
import { App } from "ionic-angular";

@Injectable()
export class SessaoProvider {

    constructor(public storage: Storage,private app:App){

    }
    // setando uma seção e passando o tipo de usuário
    create(usuario: Usuario) {
        this.storage.set('usuario', usuario);
    }

    get(): Promise<any> {
        return this.storage.get('usuario');
    }

    // Quando deslogar deve remova do storage
    remove() {
        this.storage.remove('usuario');
        const root = this.app.getRootNav();
        root.setRoot(HomePage)
        .then(()=>console.log('Logout'))
        .catch((err)=>console.log(err));
    }

    exist() {
      let exist;
        this.get().then(res => {
            console.log('resultado >>> ', res);
            if(res) {
                console.log('resultado IF');
                exist = true;
            } else {
                console.log('resultado else');
                exist =  false;
            }
        });
      return exist;
    }
}
