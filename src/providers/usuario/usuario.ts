import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { ApiProvider } from '../../providers/api/api';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  constructor(public http: Http,private apiProvider:ApiProvider) {
    console.log('Hello UsuarioProvider Provider');
  }
  public salvarUsuario(usuario):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/create-account/v1',usuario);
  }
  public pesquisarUsuario(id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+"/api/users/v1/"+id);
  }
  public pesquisarUsuarioPorNome(nome):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+"/api/users/v1/name/"+nome);
  }
  public listarAmigos(id):Observable<any>{
    return this.http.get(this.apiProvider.getUrl()+"/api/user/friends/v1/"+id);
  }
  public controlarSeguidores(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+"/api/user/friend/update/v1",data);
  }
  public editarUsuario(id,usuario):Observable<any>{
    return this.http.put(this.apiProvider.getUrl()+"/api/users/v1/"+id,usuario);
  }
  public editarImagem(usuario):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+"/api/user/updateImage/v1",usuario);
  }
  public atualizarSenha(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/update-password/v1',data);
  }
  public enviarCodigoSenha(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/forgot-password/v1',data)
  }
  public ativarConta(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/activate-email/v1',data);
  }
  public enviarCodigoEmail(data):Observable<any>{
    return this.http.post(this.apiProvider.getUrl()+'/api/verify-email/v1',data)
  }
}
