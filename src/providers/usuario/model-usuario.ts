//classe para criar modelos de objetos
export class Model {
  constructor(objeto?) {
      Object.assign(this, objeto);
  }
}
//classe usuario extendendo a classe Model
export class Usuario extends Model {
    _id: any;
    idfacebook:any;
    name: string;
    nickname:string;
    friends:any[];
    email: string;
    gender: string;
    pessoa_fisica: any;
    dob: string;
    cpf_cnpj:any;
}
